#include <QtWidgets/QApplication>

#include <Interface/Menu/mainmenu.h>

int main(int argc, char** argv){
    QApplication segu(argc, argv);

    MainMenu menu;
    menu.show();
    menu.setWindowTitle("将棋");

    return segu.exec();
}
