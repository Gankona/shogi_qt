#include "figure.h"
#include <QDebug>

Figure::Figure(QStringList &translate, QLabel *firstLabel, QWidget *menuWidget){
    trPack = &translate;
    mainX = firstLabel->x();
    mainY = firstLabel->y();
    widthLabel = firstLabel->width();

    //инициалищируем и обнуляем все фигуры
    for (int i = 0; i < 40; i++){
        interface[i] = new FigureInterface(menuWidget, firstLabel);
        interface[i]->isReturned = false;
        interface[i]->isPresent = false;
    }

    createStartPosition();
    refreshAboutSquard();
}

//обновляем информацию про то, на какой клетке что стоит
void Figure::refreshAboutSquard(){
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
            whoIsOnThisSquard[i][j] = 'n';
    for (int i = 0; i < 40; i++)
        if (interface[i]->isPresent){
            if (interface[i]->isPlayerSide)
                whoIsOnThisSquard[interface[i]->koorX][interface[i]->koorY] = 'p';
            if ( ! interface[i]->isPlayerSide)
                whoIsOnThisSquard[interface[i]->koorX][interface[i]->koorY] = 'e';
    }
}

//растановка изначальна фигур на позиции
void Figure::createStartPosition(){
    for (int i = 0; i < 40; i++){
        switch (i){
        //Описание пешек
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 20:
        case 21:
        case 22:
        case 23:
        case 24:
        case 25:
        case 26:
        case 27:
        case 28:
            interface[i]->teleportToNewPlace(i%20, ((i/20)*4)+2, true, false);
            interface[i]->nameOfFigur("歩と");
            interface[i]->kindOfFigure = 'p';
            interface[i]->kindOfReturned = 'g';
            setToolTipMethod(i, 156);
            break;

        //описание стрел
        case 9:
        case 10:
        case 29:
        case 30:
            interface[i]->teleportToNewPlace(((i%20)/10)*8, (i/20)*8, true, false);
            interface[i]->nameOfFigur("香成");
            interface[i]->kindOfFigure = 'a';
            interface[i]->kindOfReturned = 'g';
            setToolTipMethod(i, 148);
            break;

        //описание коня
        case 11:
        case 12:
        case 31:
        case 32:
            interface[i]->teleportToNewPlace(((i%10-1)*6)+1, (i/20)*8, true, false);
            interface[i]->nameOfFigur("桂成");
            interface[i]->kindOfFigure = 'h';
            interface[i]->kindOfReturned = 'g';
            setToolTipMethod(i, 140);
            break;

        //описание серебрянного генерала
        case 13:
        case 14:
        case 33:
        case 34:
            interface[i]->teleportToNewPlace(((i%20-13)*4)+2, (i/20)*8, true, false);
            interface[i]->nameOfFigur("桂成");
            interface[i]->kindOfFigure = 's';
            interface[i]->kindOfReturned = 'g';
            setToolTipMethod(i, 132);
            break;

        //описание золотого короля
        case 15:
        case 16:
        case 35:
        case 36:
            interface[i]->teleportToNewPlace(((i%20-15)*2)+3, (i/20)*8, true, false);
            interface[i]->setText("金");
            setToolTipMethod(i, 124);
            interface[i]->kindOfFigure = 'g';
            break;

        //описание короля
        case 17:
        case 37:
            interface[i]->teleportToNewPlace(4, (i/20)*8, true, false);
            interface[i]->setText("王");
            setToolTipMethod(i, 100);
            interface[i]->kindOfFigure = 'k';
            break;

        //описание слона
        case 18:
        case 38:
            interface[i]->teleportToNewPlace(((i/18)%2*6)+1, (i/20)*6+1, true, false);
            interface[i]->nameOfFigur("角馬");
            interface[i]->kindOfFigure = 'e';
            interface[i]->kindOfReturned = 'u';
            setToolTipMethod(i, 116);
            break;

        //описание ладьи
        case 19:
        case 39:
            interface[i]->teleportToNewPlace(((i/20)*6)+1, (i/20)*6+1, true, false);
            interface[i]->nameOfFigur("桂竜");
            interface[i]->kindOfFigure = 'l';
            interface[i]->kindOfReturned = 'd';
            setToolTipMethod(i, 108);
            break;
        default:;
        }
        //внесения кто с фигура по какую сторону барикад
        if (i < 20)
            interface[i]->isPlayerSide = false;
        else
            interface[i]->isPlayerSide = true;
    }
}

//установка подсказок на каждую фигуру
void Figure::setToolTipMethod(int i, int j){
    interface[i]->isPresent = true;
    if (i < 20)
        interface[i]->setStyleSheet("color: red");
    else
        interface[i]->setStyleSheet("color: green");
    interface[i]->setToolTip(trPack->at(j)+'\n'
                                          +trPack->at(j+1)+'\n'
                                          +trPack->at(j+2)+'\n'
                                          +trPack->at(j+3)+"\n \n"
                                          +trPack->at(j+4)+'\n'
                                          +trPack->at(j+5)+'\n'
                                          +trPack->at(j+6));
}

//git add -u
//git add *
//git commit -m 'Start'
//git push -u origin --all
