#include "battlectrl.h"

#include <QtCore/QDebug>
#include <QtWidgets/QMessageBox>

BattleCtrl::BattleCtrl(QStringList &translate, SeguWidget *menuWidget,
                       int typeOfGameSend, bool iAmServer, QTcpSocket *socketFromServer){
    //обнуление начальных параметров
    chooseX = -1;
    chooseY = -1;
    enemyTimeFalse = 0;
    playerTimeFalse = 0;
    isReturnedFigure = false;//правило при котором фигура что только перевернулась должна походить
    isChooseInHand = false;
    for (int i = 0; i < 18; i++){   //заполненность рукавов делаем ложью
        playerHand[i] = false;
        enemyHand[i] = false;
    }
    //сохранение переданных настроек
    typeOfGame = typeOfGameSend;
    mainWidget = menuWidget;
    translatePack = translate;
    isMyTurn = iAmServer;
    socket = socketFromServer;

    //обнуляем координаты куда можно ходить по карте
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
            chooseKoor[i][j] = false;

    //создание меню игры, фигур, и меток для отображение эфектов выделения
    interfaceBattle = new Battle(/*translatePack, */mainWidget);
    figure = new Figure(translatePack, interfaceBattle->board[0][0], interfaceBattle->mainWidget);
    toggleControl = new ToggleControl(interfaceBattle->board[0][0], mainWidget);

    if (socket != nullptr)  //если это сетевая версия тогда слушаем сокет на входящие данные
        QObject::connect(socket, SIGNAL(readyRead()), SLOT(slotReadOpponent()));
    //ловим нажатия на програме
    QObject::connect(interfaceBattle, SIGNAL(signalClickOnBoard(int,int)), SLOT(slotClickOnBoard(int,int)));
    //ловим сигналы про окончания времени хода у игрока
    QObject::connect(interfaceBattle->timerLabelPlayer, SIGNAL(signalEndOfPlayerTimer(QString)), SLOT(slotFinishTurn(QString)));
    QObject::connect(interfaceBattle->timerLabelEnemy, SIGNAL(signalEndOfPlayerTimer(QString)), SLOT(slotFinishTurn(QString)));
    slotFinishTurn("");
}

//обработка поступившей информации про нажатие на экран
void BattleCtrl::slotClickOnBoard(int xKor, int yKor){
    if (isMyTurn){  //если ход игрока
        if ((yKor == 9)&&(playerHand[xKor])){   //если нажато было в рукаве игрока
            for (int i = 0; i < 36; i++)    //очищаем всё выделенное в рукаве
                toggleControl->toggleHandLabel[i/18][i%18]->cleanAllGraphics();
            toggleControl->toggleHandLabel[1][xKor]->setChoose();   //выделеяем последнее нажатое
            isChooseInHand = true;  //говорим что была выбрана фигура в рукаве
            lastHandX = (xKor%18)/10;   //и запоминаем ее координаты
            lastHandY = (xKor%18)%10;
        }
        else if ((yKor != -1)&&(yKor != 9)){    //если нажали на доску
            lastChooseX = chooseX;  //запоминаем предыдущие
            lastChooseY = chooseY;
            chooseX = xKor; //и только нажатые координаты
            chooseY = yKor;

            switch(figure->whoIsOnThisSquard[xKor][yKor]){  //определяем на что попали
            case 'n':   //на нейтральную клетку
                if (isChooseInHand){    //проверка выделенная ли какая то фигура в рукаве
                    if(true)//проверка на возможность ходить с данной клетки
                        if(true){//еще там чето придумаю за проверку
                            for (int k = 0; k < 40; k++)
                                //ищем нужную фигуру в рукаве
                                if (((figure->interface[k]->koorX == lastHandX)
                                        &&(figure->interface[k]->koorY == lastHandY))
                                        ||(toggleControl->toggleHandLabel[k%18])){
                                    //отсылаем строку с данными в метод конец хода
                                    QString send = QString::number(lastHandX)+QString::number(lastHandY)
                                            +QString::number(chooseX)+QString::number(chooseY)+'b';
                                    playerHand[lastHandX] = false;  //освобождаем место для новых пленных
                                    slotFinishTurn(send);   //отсылаем сигнал
                                    isChooseInHand = false; //говорим что в резерве ничего не выбрано
                                    k = 40; //выход с цикла

                                    //обнуляем выделения рукавов
                                    for (int i = 0; i < 36; i++)
                                        toggleControl->toggleHandLabel[i/18][i%18]->cleanAllGraphics();
                                }
                        }
                }
                //если не было выделенного в рукаве то просто ходим выбраной фигурой в пустую клетку что доступна
                else if (chooseKoor[chooseX][chooseY]){
                    QString send = QString::number(lastChooseX)+QString::number(lastChooseY)
                            +QString::number(chooseX)+QString::number(chooseY)+'n';
                    slotFinishTurn(send);
                    isChooseInHand = false;
                }
                else    //если недоступно пойти по последним координатам то просто очистка всех выделений
                    for (int i = 0; i < 81; i++){
                        toggleControl->toggleLabel[i/9][i%9]->cleanAllGraphics();
                        chooseKoor[i/9][i%9] = false;
                        isChooseInHand = false;
                    }
                break;

            case 'e':   //если выбран враг
                if (chooseKoor[chooseX][chooseY]){  //если перед этим была выбрана фигура игрока
                    //и враг находится в зоне поражения тогда атакуем его
                    QString send = QString::number(lastChooseX)+QString::number(lastChooseY)
                            +QString::number(chooseX)+QString::number(chooseY)+'e';
                    slotFinishTurn(send);
                }
                else    //если нет тогда показать возмжный ход противника
                    showWayFromThis(false);
                isChooseInHand = false;
                break;

            case 'p':   //если нажали на игрока
                //пытаемся перевернуть фигуру
                if ((chooseKoor[chooseX][chooseY])&&(chooseY < 3)){
                        int chooseNumber;
                        isReturnedFigure = true;
                        for (int i = 0; i < 40; i++)
                            if ((figure->interface[i]->koorX == chooseX)
                                    &&(figure->interface[i]->koorY == chooseY))
                                if (figure->interface[i]->isPresent)
                                    chooseNumber = i;
                        if (! figure->interface[chooseNumber]->isReturned){
                            figure->interface[chooseNumber]->returned();
                            QString send = QString::number(lastChooseX)+QString::number(lastChooseY)
                                    +QString::number(chooseX)+QString::number(chooseY)+'r';
                            QTextStream sendStream(socket);
                            sendStream << send;
                            showWayFromThis(true);
                        }
                }   //или сменить выбранную фигуру на другую
                else if(!isReturnedFigure)
                    showWayFromThis(true);
                isChooseInHand = false;
                break;
            }
        }
    }
}

//метод поиска возможного хода у фигуры
void BattleCtrl::showWayFromThis(bool isPlayerCall){    //нужно указать игрок ли вызвал данный метод
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
            chooseKoor[i][j] = false;//обнуляем поле возможных ходов
    toggleControl->showWayFromThis(figure, isPlayerCall, chooseX, chooseY); //вызов нахождения поиска пути
    if (isPlayerCall)
        for (int i = 0; i < 9; i++) //составляем поле вохможных ходов на основе только что полученных данных
            for (int j = 0; j < 9; j++)
                chooseKoor[i][j] = toggleControl->toggleLabel[i][j]->isVisible();
}

//слот конца хода, входящий параметр строка с информацией про ход
void BattleCtrl::slotFinishTurn(QString in){
    isReturnedFigure = false;
    if (in == "endTime"){   //если закончили по вине таймера тогда накладываем штрафы
        if (isMyTurn)
            playerTimeFalse++;
        else enemyTimeFalse++;
    }
    else {  //определяем тип игры и отправляем туда строку что пришла к нам
        switch (typeOfGame){
        case 0:
            finishTurnLocalGame(in);    //на локальную игру
            break;
        case 1:
            finishTurnOneGame(/*in*/);  //на одиночную игру
            break;
        case 2:
            finishTurnTwoGame(/*in*/);  //на игру на двоих
            break;
        }
    }
    //обнуляем доску от выделений
    for (int i = 0; i < 81; i++){
        toggleControl->toggleLabel[i/9][i%9]->cleanAllGraphics();
        chooseKoor[i/9][i%9] = false;
    }
    //меняем доступность для игрока реакцию на нажитие по полю
    slotWaitingGameMod(isMyTurn);
    //меняем параметр хода на другой
    isMyTurn = !isMyTurn;
    //проверка на победу
    victoryCondition();
}

//описание локальной версии
void BattleCtrl::finishTurnLocalGame(QString in){
    if (isMyTurn){
        //если мой ход
        slotWaitingGameMod(true);
        QTextStream send(socket);   //отправка врагу информацию про мой ход
        send << in;
        if (in[4] == 'e')   //если попали на врага
            for (int i = 0; i < 40; i++)
                if (figure->interface[i]->koorX == in[2].digitValue())
                    if (figure->interface[i]->koorY == in[3].digitValue()){
                        for (int j = 0; j < 18; j++){//ищем место в рукаве для пленного и ложим его туда
                            if (!playerHand[j]){//поиск первого свободного места в рукаве
                                //он меняет при этои сторону на нашу
                                figure->interface[i]->setStyleSheet("color: green");
                                figure->interface[i]->teleportToNewPlace(j, 9, false, true);
                                playerHand[j] = true;
                                j = 18; //для выхода с цикла
                            }
                        }
                }
        for (int i = 0; i < 40; i++)    //описание возврата фигуры назад на доску
            if ((in[4] == 'b')&&(figure->interface[i]->koorY == 9)){
                if (figure->interface[i]->koorX/10 == in[0].digitValue())
                        if (figure->interface[i]->koorX%10 == in[1].digitValue()){
                                figure->interface[i]->koorX = in[2].digitValue();   //задание новых координат
                                figure->interface[i]->koorY = in[3].digitValue();   //и уствнока их в параметры фигуры
                                figure->interface[i]->teleportToNewPlace(in[2].digitValue(), in[3].digitValue(), true, true);
                            }
            }
            //или просто ход в пустую клетку
            else if ((in[4] != 'b')&&(figure->interface[i]->koorX == in[0].digitValue()))
                if (figure->interface[i]->koorY == in[1].digitValue()){
                    figure->interface[i]->koorX = in[2].digitValue();
                    figure->interface[i]->koorY = in[3].digitValue();
                    figure->interface[i]->teleportToNewPlace(in[2].digitValue(), in[3].digitValue(), true, true);
                }
    }
    else {
        //если ход опонента
        slotWaitingGameMod(false);
        if (in[4] == 'e')   //если враг тогда едим его фигуру
            for (int i = 0; i < 40; i++)
                if (figure->interface[i]->koorX == 8-in[2].digitValue())
                    if (figure->interface[i]->koorY == 8-in[3].digitValue()){
                        for (int j = 0; j < 18; j++){
                            if (!enemyHand[j]){ //ложим фигуру игрока в его рукав
                                figure->interface[i]->setStyleSheet("color: red");
                                figure->interface[i]->teleportToNewPlace(j, -1, false, false);
                                enemyHand[j] = true;
                                j = 18;
                            }
                        }
                }
        for (int i = 0; i < 40; i++)
            if ((in[4] == 'b')&&(figure->interface[i]->koorY == -1)){   //достает с рукава на доску фигуру
                if (figure->interface[i]->koorX/10 == in[0].digitValue())
                    if (figure->interface[i]->koorX%10 == in[1].digitValue()){
                        figure->interface[i]->koorX = 8-in[2].digitValue();
                        figure->interface[i]->koorY = 8-in[3].digitValue();
                        figure->interface[i]->teleportToNewPlace(8-in[2].digitValue(), 8-in[3].digitValue(), true, false);
                    }
            }
            //просто перемещает фигуру противника по доске
            else if ((in[4] != 'b')&&(figure->interface[i]->koorX == 8-in[0].digitValue()))
                if (figure->interface[i]->koorY == 8-in[1].digitValue()){
                    figure->interface[i]->koorX = 8-in[2].digitValue();
                    figure->interface[i]->koorY = 8-in[3].digitValue();
                    figure->interface[i]->teleportToNewPlace(8-in[2].digitValue(), 8-in[3].digitValue(), true, false);
                }
    }
    //обновление информации про расположение фигур
    figure->refreshAboutSquard();
}

//описание одиночной игры
void BattleCtrl::finishTurnOneGame(/*QString in*/){
    if (isMyTurn){
        //если мой ход
        slotWaitingGameMod(false);
    }
    else {
        //если ход опонента
        slotWaitingGameMod(true);
    }
    isMyTurn = !isMyTurn;
}

//описание игры на двоих
void BattleCtrl::finishTurnTwoGame(/*QString in*/){
    if (isMyTurn){
        //если мой ход
        slotWaitingGameMod(true);
    }
    else {
        //если ход опонента
        slotWaitingGameMod(false);
    }
}

//метод вкл/выкл доступности нажатий по экрану
void BattleCtrl::slotWaitingGameMod(bool isWaiting){
    if (isWaiting){
        interfaceBattle->timerLabelPlayer->setStopValue();
        interfaceBattle->timerLabelEnemy->setStartValue();
    }
    else {
        interfaceBattle->timerLabelPlayer->setStartValue();
        interfaceBattle->timerLabelEnemy->setStopValue();
    }
}

//слот чтения поступающей информации от противник в случае сетевой игры
void BattleCtrl::slotReadOpponent(){
    if (!isMyTurn){
        QTextStream tStream(socket);
        QString post = tStream.readAll();
        if ((post.at(4) == 'n')||(post.at(4) == 'e')||(post.at(4) == 'b'))//если ход тогда отправляем его в обработку
            slotFinishTurn(post);
        else if (post.at(4) == 'r'){    //если перевернуть фигуру тогда переварачиваем ёё
            for (int i = 0; i < 40; i++)
                if (figure->interface[i]->koorX == 8-post[2].digitValue())
                    if (figure->interface[i]->koorY == 8-post[3].digitValue()){
                        figure->interface[i]->returned();
                    }
        }
    }
}

//проверка на победу
void BattleCtrl::victoryCondition(){
    bool winStat = true;    //параметр, если какоето из условий сработает тогда останется истина
    QString infoWin;
    if (figure->interface[17]->isPlayerSide)    //живой ли король опонента
         infoWin ="YOU WIN";
    else if (!figure->interface[37]->isPlayerSide)  //живой ли король игрока
         infoWin = "YOU LOSE";
    else if (playerTimeFalse > 2)   //если 3 штрафа по времени то игрок проиграл
         infoWin = "YOU LOSE TIME";
    else if (enemyTimeFalse > 2)    //если 3 штрафа по времени то опонент проиграл
         infoWin = "YOU WIN TIME";
    else winStat = false;
    if (winStat){   //если была победа одного из них тогда высвечивается окно с оповещением
        QMessageBox msgBox;
        msgBox.setText(infoWin);
        msgBox.exec();

        //остановка таймеров в игре
        interfaceBattle->timerLabelEnemy->setStopValue();
        interfaceBattle->timerLabelPlayer->setStopValue();
    }
}
