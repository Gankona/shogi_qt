#ifndef FIGURE_H
#define FIGURE_H

#include <Interface/Battle/figureinterface.h>

#include <QtWidgets/QLabel>

class Figure{
public:
    Figure(QStringList &translate, QLabel *firstLabel, QWidget *menuWidget);
    FigureInterface *interface[40]; //масив 40 фигур

    QStringList *trPack;    //языковой пакет

    int mainX,  //координаты первой фигуры по Х
        mainY,  //по У
        widthLabel; //ширина фигуры
    char whoIsOnThisSquard[9][9];   //масив размещения фигур(0-нет, 1-игрок, 2-враг)

    void createStartPosition(); //метод генерации начальных значений доски
    void setToolTipMethod(int i, int j);    //метод установки подсказок
    void refreshAboutSquard();  //метод обновления информации по расположению
};

#endif // FIGURE_H
