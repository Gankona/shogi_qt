#ifndef BATTLECTRL_H
#define BATTLECTRL_H

#include <Control/figure.h>
#include <Control/togglecontrol.h>
#include <Interface/Battle/battle.h>

#include <QtCore/QObject>
#include <QtNetwork/QTcpSocket>

class BattleCtrl : public QObject{
    Q_OBJECT
public:
    BattleCtrl(QStringList &translate, SeguWidget *menuWidget,
               int typeOfGameSend, bool iAmServer, QTcpSocket *socketFromServer);
    Battle *interfaceBattle;    //создает интерйфейс поля боя
    Figure *figure; //создает фигуры и управление размещением их на доске
    ToggleControl *toggleControl;   //создает полупрозрачные выделения для отображение ходов
    SeguWidget *mainWidget; //указатель на главный обьект приложения

    QStringList translatePack;  //языковой пакет с которого извлекаются названия
    QTcpSocket *socket; //сокет для соединения клиента-сервера

    bool chooseKoor[9][9],  //матрица отображения возможных ходов
        isChooseInHand, //выбрано ли что то с рукава
        isMyTurn,   //ход игрока
        isReturnedFigure,   //перевернута ли фигура
        enemyHand[18],  //есть ли враги в рукаве
        playerHand[18]; //есть ли у игрока фигуры в рукаве
    int chooseX,    //выбрана текущая клетка в Х координате
        chooseY,    //в У координате
        lastChooseX,    //последняя выбранная клетка по Х
        lastChooseY,    //по У
        lastHandX,  //последняя выбранная фигура в рукаве по Х
        lastHandY,  //по У
        typeOfGame, //тип игры(0-сетевая, 1-один игрок, 2-два игрока)
        enemyTimeFalse, //количество просроченых ходов у противника
        playerTimeFalse;    //у игрока

    void finishTurnOneGame();   //метод обработки конца хода в одиночной игре
    void finishTurnLocalGame(QString in);   //метод обработки конца хода в сетевой игре
    void finishTurnTwoGame();   //метод обработки конца хода в игре на двоих
    void showWayFromThis(bool isPlayerCall);    //метод вызова функции отображения возможных ходов
    void victoryCondition();    //метод обработки условий победы

public slots:
    void slotClickOnBoard(int xKor, int yKor);  //слот реакции при нажатии кнопки и обработки результата
    void slotFinishTurn(QString in);    //слот конца хода
    void slotReadOpponent();    //слот для чтения входящей информации от противника при сетевой игре
    void slotWaitingGameMod(bool isWaiting);    //вкл\выкл возможность кликабельности доски
};

#endif // BATTLECTRL_H
