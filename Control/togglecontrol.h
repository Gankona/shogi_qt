#ifndef TOGGLECONTROL_H
#define TOGGLECONTROL_H

#include <Control/figure.h>
#include <Interface/Battle/togglepressway.h>

class ToggleControl{
public:
    ToggleControl(QLabel *firstLabel, QWidget *menuWidget);
    TogglePressWay *toggleHandLabel[2][18], //метка тумана рукавов
                    *toggleLabel[9][9]; //метка отображения доски 9х9

    //метод отображения возможных ходов на карте
    void showWayFromThis(Figure *figure, bool isPlayerCall, int chooseX, int chooseY);
};

#endif // TOGGLECONTROL_H
