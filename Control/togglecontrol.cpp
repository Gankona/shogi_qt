#include "togglecontrol.h"

ToggleControl::ToggleControl(QLabel *firstLabel, QWidget *menuWidget){
    //создаем 81 на поле и 36 невилимых метки для подсветки
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++){
            toggleLabel[i][j] = new TogglePressWay(menuWidget, firstLabel, i, j, true);
        }
    for (int i = 0; i < 36; i++)
        toggleHandLabel[i/18][i%18] = new TogglePressWay(menuWidget, firstLabel, i%18 , i/18, false);
}

//масив обнаружения возмодных путей и их отображение
void ToggleControl::showWayFromThis(Figure *figure, bool isPlayerCall, int chooseX, int chooseY){
    //скидываем всё выделенное
    for (int i = 0; i < 81; i++)
        toggleLabel[i/9][i%9]->cleanAllGraphics();

    //подсветка фигуры что вызвала жтот процесс
    if (!isPlayerCall)
        toggleLabel[chooseX][chooseY]->setEnemy();
    else toggleLabel[chooseX][chooseY]->setChoose();

    //ищем кто с 40 фигур вызвал данный метод
    int chooseNumber;
    for (int i = 0; i < 40; i++)
        if ((figure->interface[i]->koorX == chooseX)
                &&(figure->interface[i]->koorY == chooseY))
            if (figure->interface[i]->isPresent)
                chooseNumber = i;

    //определяем кто враг что бы его включить в путь, а фигуры игрока на пути исключить
    char side;
    if (isPlayerCall)
        side = 'e';
    else side = 'p';


    //по значению фигуры ищем возмодные пути
    switch(figure->interface[chooseNumber]->kindOfFigure){

    //p - пехотинец(пешка)
    case 'p':
        int per;
        if (figure->whoIsOnThisSquard[chooseX][chooseY] == 'e')
            per = 1;
        else per = -1;
        if (figure->whoIsOnThisSquard[chooseX][chooseY+per] == 'n')
            toggleLabel[chooseX][chooseY+per]->setFreeWay();
        if (figure->whoIsOnThisSquard[chooseX][chooseY+per] == 'e')
            toggleLabel[chooseX][chooseY+per]->setEnemy();
        break;

    //s - серебрянный генерал
    case 's':
        if ((chooseY != 0)&&(side == 'e')){
            if (figure->whoIsOnThisSquard[chooseX][chooseY-1] == side)
                toggleLabel[chooseX][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX][chooseY-1] == 'n')
                toggleLabel[chooseX][chooseY-1]->setFreeWay();
        }
        if ((chooseX != 0)&&(chooseY != 0)){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY-1] == side)
                toggleLabel[chooseX-1][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY-1] == 'n')
                toggleLabel[chooseX-1][chooseY-1]->setFreeWay();
        }
        if ((chooseY != 8)&&(side == 'p')){
            if (figure->whoIsOnThisSquard[chooseX][chooseY+1] == side)
                toggleLabel[chooseX][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX][chooseY+1] == 'n')
                toggleLabel[chooseX][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 8)&&(chooseY != 8)){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY+1] == side)
                toggleLabel[chooseX+1][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY+1] == 'n')
                toggleLabel[chooseX+1][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 0)&&(chooseY != 8)){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY+1] == side)
                toggleLabel[chooseX-1][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY+1] == 'n')
                toggleLabel[chooseX-1][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 8)&&(chooseY != 0)){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY-1] == side)
                toggleLabel[chooseX+1][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY-1] == 'n')
                toggleLabel[chooseX+1][chooseY-1]->setFreeWay();
        }
        break;

    //g - золотой генерал
    case 'g':
        if (chooseX != 0){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY] == side)
                toggleLabel[chooseX-1][chooseY]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY] == 'n')
                toggleLabel[chooseX-1][chooseY]->setFreeWay();
        }
        if (chooseY != 0){
            if (figure->whoIsOnThisSquard[chooseX][chooseY-1] == side)
                toggleLabel[chooseX][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX][chooseY-1] == 'n')
                toggleLabel[chooseX][chooseY-1]->setFreeWay();
        }
        if ((chooseX != 0)&&(chooseY != 0)&&(side == 'e')){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY-1] == side)
                toggleLabel[chooseX-1][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY-1] == 'n')
                toggleLabel[chooseX-1][chooseY-1]->setFreeWay();
        }
        if (chooseX != 8){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY] == side)
                toggleLabel[chooseX+1][chooseY]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY] == 'n')
                toggleLabel[chooseX+1][chooseY]->setFreeWay();
        }
        if (chooseY != 8){
            if (figure->whoIsOnThisSquard[chooseX][chooseY+1] == side)
                toggleLabel[chooseX][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX][chooseY+1] == 'n')
                toggleLabel[chooseX][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 8)&&(chooseY != 8)&&(side == 'p')){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY+1] == side)
                toggleLabel[chooseX+1][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY+1] == 'n')
                toggleLabel[chooseX+1][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 0)&&(chooseY != 8)&&(side == 'p')){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY+1] == side)
                toggleLabel[chooseX-1][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY+1] == 'n')
                toggleLabel[chooseX-1][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 8)&&(chooseY != 0)&&(side == 'e')){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY-1] == side)
                toggleLabel[chooseX+1][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY-1] == 'n')
                toggleLabel[chooseX+1][chooseY-1]->setFreeWay();
        }
        break;

    //h - колесница(конь)
    case 'h':
        int re;
        if (side == 'p')
            re = 2;
        else re = -2;
        if (((side == 'e')&&(chooseY > 1))||((side == 'p')&&(chooseY < 7))){
            if (chooseX != 0){
                if (figure->whoIsOnThisSquard[chooseX-1][chooseY+re] == side)
                    toggleLabel[chooseX-1][chooseY+re]->setEnemy();
                else if (figure->whoIsOnThisSquard[chooseX-1][chooseY+re] == 'n')
                    toggleLabel[chooseX-1][chooseY+re]->setFreeWay();
            }
            if (chooseX != 8){
                if (figure->whoIsOnThisSquard[chooseX+1][chooseY+re] == side)
                    toggleLabel[chooseX+1][chooseY+re]->setEnemy();
                else if (figure->whoIsOnThisSquard[chooseX+1][chooseY+re] == 'n')
                    toggleLabel[chooseX+1][chooseY+re]->setFreeWay();
            }
        }
        break;

    //e - слон
    case 'e':
        for (int i = chooseX, j = chooseY;(i > 0)&&(j > 0); i--, j--){
            if (figure->whoIsOnThisSquard[i-1][j-1] == side){
                toggleLabel[i-1][j-1]->setEnemy();
                i = 0;
            }
            else if (figure->whoIsOnThisSquard[i-1][j-1] == 'n')
                toggleLabel[i-1][j-1]->setFreeWay();
            else i = 0;
        }
        for (int i = chooseX, j = chooseY;(i < 8)&&(j > 0); i++, j--){
            if (figure->whoIsOnThisSquard[i+1][j-1] == side){
                toggleLabel[i+1][j-1]->setEnemy();
                i = 8;
            }
            else if (figure->whoIsOnThisSquard[i+1][j-1] == 'n')
                toggleLabel[i+1][j-1]->setFreeWay();
            else i = 8;
        }
        for (int i = chooseX, j = chooseY;(i < 8)&&(j < 8); i++, j++){
            if (figure->whoIsOnThisSquard[i+1][j+1] == side){
                toggleLabel[i+1][j+1]->setEnemy();
                i = 8;
            }
            else if (figure->whoIsOnThisSquard[i+1][j+1] == 'n')
                toggleLabel[i+1][j+1]->setFreeWay();
            else i = 8;
        }
        for (int i = chooseX, j = chooseY;(i > 0)&&(j < 8); i--, j++){
            if (figure->whoIsOnThisSquard[i-1][j+1] == side){
                toggleLabel[i-1][j+1]->setEnemy();
                i = 0;
            }
            else if (figure->whoIsOnThisSquard[i-1][j+1] == 'n')
                toggleLabel[i-1][j+1]->setFreeWay();
            else i = 0;
        }
        break;

    //l - ладья
    case 'l':
        for (int i = chooseX; i > 0; i--){
            if (figure->whoIsOnThisSquard[i-1][chooseY] == side){
                toggleLabel[i-1][chooseY]->setEnemy();
                i = 0;
            }
            else if (figure->whoIsOnThisSquard[i-1][chooseY] == 'n')
                toggleLabel[i-1][chooseY]->setFreeWay();
            else i = 0;
        }
        for (int i = chooseX; i < 8; i++){
            if (figure->whoIsOnThisSquard[i+1][chooseY] == side){
                toggleLabel[i+1][chooseY]->setEnemy();
                i = 8;
            }
            else if (figure->whoIsOnThisSquard[i+1][chooseY] == 'n')
                toggleLabel[i+1][chooseY]->setFreeWay();
            else i = 8;
        }
        for (int i = chooseY; i < 8; i++){
            if (figure->whoIsOnThisSquard[chooseX][i+1] == side){
                toggleLabel[chooseX][i+1]->setEnemy();
                i = 8;
            }
            else if (figure->whoIsOnThisSquard[chooseX][i+1] == 'n')
                toggleLabel[chooseX][i+1]->setFreeWay();
            else i = 8;
        }
        for (int i = chooseY;(i > 0); i--){
            if (figure->whoIsOnThisSquard[chooseX][i-1] == side){
                toggleLabel[chooseX][i-1]->setEnemy();
                i = 0;
            }
            else if (figure->whoIsOnThisSquard[chooseX][i-1] == 'n')
                toggleLabel[chooseX][i-1]->setFreeWay();
            else i = 0;
        }
        break;

    //a - стрела
    case 'a':
        if (figure->whoIsOnThisSquard[chooseX][chooseY] == 'e'){
            for (int i = chooseY; i < 8; i++){
                if(figure->whoIsOnThisSquard[chooseX][i+1] == 'p')
                    toggleLabel[chooseX][i+1]->setEnemy();
                if (figure->whoIsOnThisSquard[chooseX][i+1] != 'n')
                    i = 8;
                else
                    toggleLabel[chooseX][i+1]->setFreeWay();
            }
        }
        else {
            for (int i = chooseY; i > 0; i--){
                if(figure->whoIsOnThisSquard[chooseX][i-1] == 'e')
                    toggleLabel[chooseX][i-1]->setEnemy();
                if (figure->whoIsOnThisSquard[chooseX][i-1] != 'n')
                    i = 0;
                else
                    toggleLabel[chooseX][i-1]->setFreeWay();
            }
        }
        break;

    //k - король
    case 'k':
        if (chooseX != 0){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY] == side)
                toggleLabel[chooseX-1][chooseY]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY] == 'n')
                toggleLabel[chooseX-1][chooseY]->setFreeWay();
        }
        if (chooseY != 0){
            if (figure->whoIsOnThisSquard[chooseX][chooseY-1] == side)
                toggleLabel[chooseX][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX][chooseY-1] == 'n')
                toggleLabel[chooseX][chooseY-1]->setFreeWay();
        }
        if ((chooseX != 0)&&(chooseY != 0)){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY-1] == side)
                toggleLabel[chooseX-1][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY-1] == 'n')
                toggleLabel[chooseX-1][chooseY-1]->setFreeWay();
        }
        if (chooseX != 8){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY] == side)
                toggleLabel[chooseX+1][chooseY]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY] == 'n')
                toggleLabel[chooseX+1][chooseY]->setFreeWay();
        }
        if (chooseY != 8){
            if (figure->whoIsOnThisSquard[chooseX][chooseY+1] == side)
                toggleLabel[chooseX][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX][chooseY+1] == 'n')
                toggleLabel[chooseX][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 8)&&(chooseY != 8)){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY+1] == side)
                toggleLabel[chooseX+1][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY+1] == 'n')
                toggleLabel[chooseX+1][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 0)&&(chooseY != 8)){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY+1] == side)
                toggleLabel[chooseX-1][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY+1] == 'n')
                toggleLabel[chooseX-1][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 8)&&(chooseY != 0)){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY-1] == side)
                toggleLabel[chooseX+1][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY-1] == 'n')
                toggleLabel[chooseX+1][chooseY-1]->setFreeWay();
        }
        break;

    //лошадь дракон
    case 'u':
        for (int i = chooseX, j = chooseY;(i > 0)&&(j > 0); i--, j--){
            if (figure->whoIsOnThisSquard[i-1][j-1] == side){
                toggleLabel[i-1][j-1]->setEnemy();
                i = 0;
            }
            else if (figure->whoIsOnThisSquard[i-1][j-1] == 'n')
                toggleLabel[i-1][j-1]->setFreeWay();
            else i = 0;
        }
        for (int i = chooseX, j = chooseY;(i < 8)&&(j > 0); i++, j--){
            if (figure->whoIsOnThisSquard[i+1][j-1] == side){
                toggleLabel[i+1][j-1]->setEnemy();
                i = 8;
            }
            else if (figure->whoIsOnThisSquard[i+1][j-1] == 'n')
                toggleLabel[i+1][j-1]->setFreeWay();
            else i = 8;
        }
        for (int i = chooseX, j = chooseY;(i < 8)&&(j < 8); i++, j++){
            if (figure->whoIsOnThisSquard[i+1][j+1] == side){
                toggleLabel[i+1][j+1]->setEnemy();
                i = 8;
            }
            else if (figure->whoIsOnThisSquard[i+1][j+1] == 'n')
                toggleLabel[i+1][j+1]->setFreeWay();
            else i = 8;
        }
        for (int i = chooseX, j = chooseY;(i > 0)&&(j < 8); i--, j++){
            if (figure->whoIsOnThisSquard[i-1][j+1] == side){
                toggleLabel[i-1][j+1]->setEnemy();
                i = 0;
            }
            else if (figure->whoIsOnThisSquard[i-1][j+1] == 'n')
                toggleLabel[i-1][j+1]->setFreeWay();
            else i = 0;
        }
        if (chooseX != 0){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY] == side)
                toggleLabel[chooseX-1][chooseY]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY] == 'n')
                toggleLabel[chooseX-1][chooseY]->setFreeWay();
        }
        if (chooseY != 0){
            if (figure->whoIsOnThisSquard[chooseX][chooseY-1] == side)
                toggleLabel[chooseX][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX][chooseY-1] == 'n')
                toggleLabel[chooseX][chooseY-1]->setFreeWay();
        }
        if ((chooseX != 0)&&(chooseY != 0)){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY-1] == side)
                toggleLabel[chooseX-1][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY-1] == 'n')
                toggleLabel[chooseX-1][chooseY-1]->setFreeWay();
        }
        if (chooseX != 8){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY] == side)
                toggleLabel[chooseX+1][chooseY]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY] == 'n')
                toggleLabel[chooseX+1][chooseY]->setFreeWay();
        }
        if (chooseY != 8){
            if (figure->whoIsOnThisSquard[chooseX][chooseY+1] == side)
                toggleLabel[chooseX][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX][chooseY+1] == 'n')
                toggleLabel[chooseX][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 8)&&(chooseY != 8)){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY+1] == side)
                toggleLabel[chooseX+1][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY+1] == 'n')
                toggleLabel[chooseX+1][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 0)&&(chooseY != 8)){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY+1] == side)
                toggleLabel[chooseX-1][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY+1] == 'n')
                toggleLabel[chooseX-1][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 8)&&(chooseY != 0)){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY-1] == side)
                toggleLabel[chooseX+1][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY-1] == 'n')
                toggleLabel[chooseX+1][chooseY-1]->setFreeWay();
        }

    //дракон
    case 'd':
        if (chooseX != 0){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY] == side)
                toggleLabel[chooseX-1][chooseY]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY] == 'n')
                toggleLabel[chooseX-1][chooseY]->setFreeWay();
        }
        if (chooseY != 0){
            if (figure->whoIsOnThisSquard[chooseX][chooseY-1] == side)
                toggleLabel[chooseX][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX][chooseY-1] == 'n')
                toggleLabel[chooseX][chooseY-1]->setFreeWay();
        }
        if ((chooseX != 0)&&(chooseY != 0)){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY-1] == side)
                toggleLabel[chooseX-1][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY-1] == 'n')
                toggleLabel[chooseX-1][chooseY-1]->setFreeWay();
        }
        if (chooseX != 8){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY] == side)
                toggleLabel[chooseX+1][chooseY]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY] == 'n')
                toggleLabel[chooseX+1][chooseY]->setFreeWay();
        }
        if (chooseY != 8){
            if (figure->whoIsOnThisSquard[chooseX][chooseY+1] == side)
                toggleLabel[chooseX][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX][chooseY+1] == 'n')
                toggleLabel[chooseX][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 8)&&(chooseY != 8)){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY+1] == side)
                toggleLabel[chooseX+1][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY+1] == 'n')
                toggleLabel[chooseX+1][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 0)&&(chooseY != 8)){
            if (figure->whoIsOnThisSquard[chooseX-1][chooseY+1] == side)
                toggleLabel[chooseX-1][chooseY+1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX-1][chooseY+1] == 'n')
                toggleLabel[chooseX-1][chooseY+1]->setFreeWay();
        }
        if ((chooseX != 8)&&(chooseY != 0)){
            if (figure->whoIsOnThisSquard[chooseX+1][chooseY-1] == side)
                toggleLabel[chooseX+1][chooseY-1]->setEnemy();
            else if (figure->whoIsOnThisSquard[chooseX+1][chooseY-1] == 'n')
                toggleLabel[chooseX+1][chooseY-1]->setFreeWay();
        }
        for (int i = chooseX; i > 0; i--){
            if (figure->whoIsOnThisSquard[i-1][chooseY] == side){
                toggleLabel[i-1][chooseY]->setEnemy();
                i = 0;
            }
            else if (figure->whoIsOnThisSquard[i-1][chooseY] == 'n')
                toggleLabel[i-1][chooseY]->setFreeWay();
            else i = 0;
        }
        for (int i = chooseX; i < 8; i++){
            if (figure->whoIsOnThisSquard[i+1][chooseY] == side){
                toggleLabel[i+1][chooseY]->setEnemy();
                i = 8;
            }
            else if (figure->whoIsOnThisSquard[i+1][chooseY] == 'n')
                toggleLabel[i+1][chooseY]->setFreeWay();
            else i = 8;
        }
        for (int i = chooseY; i < 8; i++){
            if (figure->whoIsOnThisSquard[chooseX][i+1] == side){
                toggleLabel[chooseX][i+1]->setEnemy();
                i = 8;
            }
            else if (figure->whoIsOnThisSquard[chooseX][i+1] == 'n')
                toggleLabel[chooseX][i+1]->setFreeWay();
            else i = 8;
        }
        for (int i = chooseY;(i > 0); i--){
            if (figure->whoIsOnThisSquard[chooseX][i-1] == side){
                toggleLabel[chooseX][i-1]->setEnemy();
                i = 0;
            }
            else if (figure->whoIsOnThisSquard[chooseX][i-1] == 'n')
                toggleLabel[chooseX][i-1]->setFreeWay();
            else i = 0;
        }
    }
}
