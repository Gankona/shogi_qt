#ifndef SERVER_H
#define SERVER_H

#include <Control/battlectrl.h>

#include <QtCore/QObject>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QNetworkInterface>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

class Server : public QObject{
    Q_OBJECT
public:
    Server(QObject *parent, QString nameServer, QStringList &translate, SeguWidget *menuWidget);
    BattleCtrl *battleGame; //ссылка на создание будущей игры
    SeguWidget *mainWidget; //ссылка на главный виджет

    QHostAddress myAddresIP;    //хост адрес данного устройства
    QList<QTcpSocket*> socketList;  //список всех соединений в сети
    QTcpServer *server; //сервер
    QString nameOfServer;   //имя созданного сервера
    QStringList translatePack;  //языковой пакет

public slots:
    void connectToHosti();  //вызывается при попытке подсоединится к нашему серверу
    void slotReadClient();  //вызывается если клиент отправляет на сервер информацию
};

#endif // SERVER_H
