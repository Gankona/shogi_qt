#include "server.h"

Server::Server(QObject *parent, QString nameServer, QStringList &translate, SeguWidget *menuWidget){
    nameOfServer = nameServer;
    mainWidget = menuWidget;
    translatePack = translate;
    //ищем свой ip
    QList<QHostAddress> list = QNetworkInterface::allAddresses();
    if (list.length() > 1)
        myAddresIP = list.at(2);
    qDebug() << list;
    socketList.clear();

    //создание сервера что слушает порт 42015
    server = new QTcpServer(parent);
    server->listen(QHostAddress::Any, 42015);
    QObject::connect(server, SIGNAL(newConnection()), SLOT(connectToHosti()));
}

//вызывается если кто то захочет подключится к сервера и возвращает имя сервера
void Server::connectToHosti(){
    socketList.push_back(new QTcpSocket);
    socketList.last() = server->nextPendingConnection();
    QObject::connect(socketList.last(), SIGNAL(readyRead()), SLOT(slotReadClient()));
    QTextStream os(socketList.last());
    os << nameOfServer;
}

//вызывается если кто то с клиентов отправляет данные на сервер
void Server::slotReadClient(){
    QTcpSocket *select = (QTcpSocket*)sender();
    qDebug() << "что то пришло от игрока на сервер";
    QTextStream reading(select);
    QString command = reading.readAll();
    qDebug() << "Игра начинается по запросу клиента";
    if (command == "start"){
        //начинается сама игра
        battleGame = new BattleCtrl(translatePack, mainWidget, 0, true, select);
        QObject::disconnect(select, SIGNAL(readyRead()), this, SLOT(slotReadClient()));
    }
}
