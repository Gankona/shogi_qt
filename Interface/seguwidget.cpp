#include "seguwidget.h"

SeguWidget::SeguWidget(QWidget *parent):QWidget(parent){}

//переопределенный метод реакции на нажатие по фиджету и возвращает координаты нажатия
void SeguWidget::mouseReleaseEvent(QMouseEvent *pe){
    emit widgetReturned(pe->pos());
}
