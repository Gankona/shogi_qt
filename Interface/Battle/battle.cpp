#include "battle.h"
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>
#include <QDebug>

Battle::Battle(SeguWidget *menuWidget):qsettings("fpro","Segu"){
    readSettings(); //читаем настройки

    //если включен полноекранный тогда создаем полнокранную версию игры
    if (isFullScreen){
        menuWidget->hide();
        mainWidget = new SeguWidget;
        createFullScreenInterface();
    }
    //иначе создается оконный режим
    else {
        mainWidget = menuWidget;
        createWindowInterface();
    }

    //слушаем где была нажата клавиша и отправляем дальше
    QObject::connect(mainWidget, SIGNAL(widgetReturned(QPoint)), SLOT(slotClickOnBoard(QPoint)));
}

//настройка интерфейса полноэкранного режима
void Battle::createFullScreenInterface(){
    razmX = QApplication::desktop()->screenGeometry().width();
    razmY = QApplication::desktop()->screenGeometry().height();
    mainBattleLabel = new QLabel(mainWidget);
    mainBattleLabel->setFixedSize(razmY, razmY);
    mainBattleLabel->move(0,0);
    mainBattleLabel->setStyleSheet("background: navy");

    //генерация таблицы 9х9
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++){
            board[i][j] = new QLabel(mainBattleLabel);
            board[i][j]->setFixedSize(razmY/12, razmY/12);
            board[i][j]->move(razmY/8-9+i*(razmY/12+2), razmY/8-9+j*(razmY/12+2));
            board[i][j]->setStyleSheet("background: khaki");
        }
    informLabel = new InformLabel(mainWidget, razmY, razmX-razmY, razmY);
    mainWidget->showFullScreen();
}

//создаем оконную версиб игры
void Battle::createWindowInterface(){
    mainBattleLabel = new QLabel(mainWidget);
    mainBattleLabel->setFixedSize(razmY*10/12, razmY);
    mainBattleLabel->move(0,0);
    mainBattleLabel->setStyleSheet("background: white");
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++){
            board[i][j] = new QLabel(mainBattleLabel);
            board[i][j]->setFixedSize(razmY/12, razmY/12);
            board[i][j]->move(razmY/24-8+i*(razmY/12+2), razmY/8-8+j*(razmY/12+2));
            board[i][j]->setStyleSheet("background: khaki");
        }
    switcherInformButton = new QPushButton(mainBattleLabel);
    switcherInformButton->setFixedSize(razmY/36-2, razmY*9/12+16);
    switcherInformButton->move(2+board[8][0]->x()+board[8][0]->width(), board[8][0]->y());
    switcherInformButton->setFocusPolicy(Qt::NoFocus);
    switcherInformButton->setStyleSheet("background: navy; color: white");
    switcherInformButton->setFlat(true);
    mainBattleLabel->show();
    //если в настройках созранена открытым помощь тогда ее открываем, иначе нет
    if (qsettings.value("/Settings/gameInterface/isShowInformLabel").toBool()){
        mainWidget->setFixedSize(razmX, razmY);
        switcherInformButton->setText("<");
    }
    else {
        mainWidget->setFixedSize(mainBattleLabel->width(), razmY);
        switcherInformButton->setText(">");
    }
    informLabel = new InformLabel(mainWidget, mainBattleLabel->width(), razmX-razmY, razmY);
    timerLabelPlayer = new TimerLabel(mainWidget, razmY, true);
    timerLabelEnemy = new TimerLabel(mainWidget, razmY, false);
    QObject::connect(switcherInformButton, SIGNAL(clicked()), SLOT(slotSwitcherInformLabel()));
}

//читаем настройки игры перед запуском
void Battle::readSettings(){
    qsettings.beginGroup("/Settings/gameInterface");
    isFullScreen = qsettings.value("/isFullScreen", "false").toBool();
    if (!isFullScreen){
        razmX = qsettings.value("/razmerGameX", "680").toInt();
        razmY = qsettings.value("/razmerGameY", "680").toInt();
    }
    qsettings.endGroup();
}

//описание кнопки вкл/выкл дополнительной информации
void Battle::slotSwitcherInformLabel(){
    bool isVisible;
    if (mainWidget->width() == mainBattleLabel->width()){
        mainWidget->setFixedSize(informLabel->width()+mainBattleLabel->width(), razmY);
        switcherInformButton->setText("<");
        isVisible = true;
    }
    else{
        mainWidget->setFixedSize(mainBattleLabel->width(), razmY);
        switcherInformButton->setText(">");
        isVisible = false;
    }
    qsettings.setValue("/Settings/gameInterface/isShowInformLabel", isVisible);
}

//ищем на какую именно клетку фигуры попало нажатие
void Battle::slotClickOnBoard(QPoint pe){
    int width = board[0][0]->width()+2;
    int height = board[0][0]->height()+2;
    bool isOnBoard = false;
    //проходим сначала доску
    for (int i = 0; i < 9; i++)
        for (int j = 0; j < 9; j++)
            if ((board[i][j]->x() < pe.x())
                    &&(board[i][j]->x()+width > pe.x())
                    &&(board[i][j]->y() < pe.y())
                    &&(board[i][j]->y()+height > pe.y())){
                emit signalClickOnBoard(i, j);
                isOnBoard = true;
            }
    if (!isOnBoard){
        //потом рукава
        for (int i = 0; i < 18; i++){
            if ((board[0][0]->y()+(-1)*(board[0][0]->width()+2) < pe.y())
                    &&(board[0][0]->y()+(-1)*(board[0][0]->width()+2)+board[0][0]->width() > pe.y())
                    &&(board[0][0]->x()+i*(board[0][0]->width()/2+2) < pe.x())
                    &&(board[0][0]->x()+i*(board[0][0]->width()/2+2)+board[0][0]->width()/2 > pe.x()))
                emit signalClickOnBoard(i, -1);
            if ((board[0][0]->y()+9*(board[0][0]->width()+2) < pe.y())
                    &&(board[0][0]->y()+9*(board[0][0]->width()+2)+board[0][0]->width() > pe.y())
                    &&(board[0][0]->x()+i*(board[0][0]->width()/2+2) < pe.x())
                    &&(board[0][0]->x()+i*(board[0][0]->width()/2+2)+board[0][0]->width()/2 > pe.x()))
                emit signalClickOnBoard(i, 9);
        }
    }
}
