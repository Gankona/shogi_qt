#include "timerlabel.h"
#include <QtCore/QTimer>
#include <QDebug>

TimerLabel::TimerLabel(QWidget *parent, int razmY, bool isPlayer, Qt::WindowFlags f) : QLabel(parent, f){
    //настройка размеров и размещения таймеров на виджете
    if (isPlayer)
        this->move(razmY/24-8, razmY/8-8+9*(razmY/12+2));
    else this->move(razmY/24-8, razmY/8-10-razmY/96);
    this->setFixedSize(9*(razmY/12+2), razmY/96);

    widthSize = 9*(razmY/12+2); //запоминаем максимальную ширину поломы таймера
    isStop = false; //задаем значение стоп данному таймеру

    this->show(); //перерисовать виджет
}

//начать таймер сделав его зеленым
void TimerLabel::setStartValue(){
    percent = 100;
    green = 255;
    red = 0;
    isStop = false; //установить что таймер идет
    this->setFixedWidth(widthSize);
    QTimer::singleShot(100, this, SLOT(slotTimerShift()));//через каждые 100 мс вызывать движение
}

void TimerLabel::slotTimerShift(){
    if (percent > 0){//пока таймер не достигент конца уменшаем длину таймера
        QTimer::singleShot(100, this, SLOT(slotTimerShift()));
        this->setFixedWidth(percent*widthSize/100);
        this->setStyleSheet("background: rgb("+QString::number(red)+","
                            +QString::number(green)+",0)");
        //меняем цвет у таймера
        if ((percent < 50)&&(percent > 25))
            red = (int)10*(50-percent);
        if (percent < 25)
            green = percent*10;

        percent -= 0.04;
    }
    else
        if (!isStop)
            emit signalEndOfPlayerTimer("endTime"); //говорим что время кончилось в управление игрой
}

//останавливаем таймер
void TimerLabel::setStopValue(){
    isStop = true;
    percent = -10;
    green = 255;
    red = 0;
    this->setFixedWidth(1);
}
