#include "togglepressway.h"
#include <QDebug>

TogglePressWay::TogglePressWay(QWidget *parent, QLabel *forSizes,
                               int i, int j, bool isOnBoard, Qt::WindowFlags f) : QLabel(parent, f){
    if (isOnBoard){//если фигура должна будет стоять на доске
        this->setFixedSize(forSizes->size());
        this->move(forSizes->x()+i*(forSizes->width()+2), forSizes->y()+j*(forSizes->height()+2));
    }
    else {//если фигура уходит в рукав
        if (j == 0){//если она уходит в рукав противнику
            this->setFixedSize(forSizes->width()/2, forSizes->height());
            this->move(forSizes->x()+i*(forSizes->width()/2+2), forSizes->y()+(-1)*(forSizes->height()+2));
        }
        else {//уходит в рукав игрока
            this->setFixedSize(forSizes->width()/2, forSizes->height());
            this->move(forSizes->x()+i*(forSizes->width()/2+2), forSizes->y()+9*(forSizes->height()+2));
            this->setFreeWay();
        }
    }
    this->setStyleSheet("background: white");
    this->show();

    //настройка прозрачности для данного виджета
    opacity = new QGraphicsOpacityEffect;
    opacity->setOpacity(0.35);
    this->setGraphicsEffect(opacity);
    this->cleanAllGraphics();
}

//очистка выделения фигуры определенным цветом
void TogglePressWay::cleanAllGraphics(){
    this->setVisible(false);
}

//обозначить как свбодный путь
void TogglePressWay::setFreeWay(){
    this->setStyleSheet("background: green");
    this->setVisible(true);
}

//обозначить выбранную клетку
void TogglePressWay::setChoose(){
    this->setStyleSheet("background: navy");
    this->setVisible(true);
}

//обозначить врага на карте
void TogglePressWay::setEnemy(){
    this->setStyleSheet("background: darkred");
    this->setVisible(true);
}

