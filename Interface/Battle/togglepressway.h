#ifndef TOGGLEPRESSWAY_H
#define TOGGLEPRESSWAY_H

#include <QtWidgets/QGraphicsOpacityEffect>
#include <QtWidgets/QLabel>

class TogglePressWay : public QLabel{
public:
    TogglePressWay(QWidget *parent, QLabel *forSizes, int i, int j, bool isOnBoard, Qt::WindowFlags f=0);
    QGraphicsOpacityEffect *opacity;    //прозрачность

    void cleanAllGraphics();    //очистка прозрачности
    void setFreeWay();  //отображение свободного пути
    void setEnemy();    //отображение если на пути враг
    void setChoose();   //отображение выбранной клетки
};

#endif // TOGGLEPRESSWAY_H
