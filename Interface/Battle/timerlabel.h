#ifndef TIMERLABEL_H
#define TIMERLABEL_H

#include <QtWidgets/QLabel>

class TimerLabel : public QLabel{
    Q_OBJECT
public:
    TimerLabel(QWidget *parent, int razmY, bool isPlayer, Qt::WindowFlags f=0);

    bool isStop;    //остановлен таймер или идет
    int widthSize,  //максимальная ширина таймера
        red,    //насыщенность красным
        green;  //зеленым
    float percent;  //процент размера от максимального

    void setStartValue();   //метод запуска таймера
    void setStopValue();    //метод остановки таймера

public slots:
    void slotTimerShift();  //слот движения таймера

signals:
    void signalEndOfPlayerTimer(QString);   //вызывается при истечении времени
};

#endif // TIMERLABEL_H
