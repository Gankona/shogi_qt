#include "informlabel.h"

InformLabel::InformLabel(QWidget *parent, int moveX, int razmX, int razmY, Qt::WindowFlags f) : QLabel(parent, f){
    setFixedSize(razmX, razmY);
    move(moveX, 0);
    setStyleSheet("background: greenyellow");
    show();
}
