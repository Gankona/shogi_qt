#include "figureinterface.h"
#include <QDebug>

FigureInterface::FigureInterface(QWidget *parent, QLabel *label, Qt::WindowFlags f)
                                    : QLabel(parent, f){
    size = label->size();
    pointFirst = label->pos();

    this->setFixedSize(size);
    this->setAlignment(Qt::AlignCenter);
    this->show();
}

//p - пехотинец(пешка)
//s - серебрянный генерал
//g - золотой генерал
//h - колесница(конь)
//e - слон
//l - ладья
//a - стрела
//k - король
//u - лошадь дракон
//d - дракон

//метод перемещает данную фигуру в нужное место
//первые два параметра это место в матрице доски, если onBoard == true
void FigureInterface::teleportToNewPlace(int i, int j, bool onBoard, bool playerSide){
    if (onBoard){
        this->isPlayerSide = playerSide;
        this->isPresent = true;
        this->move(pointFirst.x()+i*(size.width()+2), pointFirst.y()+j*(size.height()+2));
        this->setFixedWidth(size.width());
        koorX = i;
        koorY = j;
    }
    else{//если фигура лежит в рукаве
        this->isPresent = false;
        if (isReturned){//если она была перевернута то делаем ее стандартной
            char per = kindOfFigure;
            kindOfFigure = kindOfReturned;
            kindOfReturned = per;
            this->setText(names.at(0));
            isReturned = false;
        }
        this->setFixedWidth(size.width()/2);
        this->move(pointFirst.x()+i*(size.width()/2+2), pointFirst.y()+j*(size.height()+2));
        koorX = i;
        koorY = j;
        isPlayerSide = playerSide; //запоминаем на чьей стороне играет фигура
    }
}

//запоминаем названия фигур тут
void FigureInterface::nameOfFigur(QString str){
    names = str;
    this->setText(str.at(0));
}

//перевертываем фигуру
void FigureInterface::returned(){
    if (!isReturned){
        isReturned = true;
        this->setText(names.at(1));
    }
    else {
        isReturned = false;
        this->setText(names.at(0));
    }
    //меняем местами главную часть и перевернутую(по умол. главная)
    char per = kindOfFigure;
    kindOfFigure = kindOfReturned;
    kindOfReturned = per;
}
