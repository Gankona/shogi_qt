#ifndef INFORMLABEL_H
#define INFORMLABEL_H

#include <QtWidgets/QLabel>

class InformLabel : public QLabel{
    Q_OBJECT
public:
    InformLabel(QWidget *parent, int move, int razmX, int razmY, Qt::WindowFlags f=0);
};

#endif // INFORMLABEL_H
