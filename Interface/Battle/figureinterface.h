#ifndef FIGUREINTERFACE_H
#define FIGUREINTERFACE_H

#include <QtWidgets/QLabel>

class FigureInterface : public QLabel{
public:
    FigureInterface(QWidget *menuWidget, QLabel *label, Qt::WindowFlags f=0);

    QPoint pointFirst;  //координаты первой метки с доски
    QSize size; //размеры клетки на доске
    QString names;  //название обычной и перевернутой фигуры
    QWidget *mainWindow;    //ссылка на главный обьект


    bool isReturned,    //перевернутая или нет
        isPlayerSide,   //сторона на которой играет
        isPresent;  //есть ли на карте
    char kindOfFigure,  //тип фигуры
        kindOfReturned; //тип фигуры если она перевернута
    int koorX, koorY;  //координаты фигуры

    void nameOfFigur(QString str);  //метод обновления названия фигуры
    void returned();    //метод перевертывания фигуры
    //перемещение фигуры в новое место
    void teleportToNewPlace(int i, int j, bool onBoard, bool playerSide);
};

#endif // FIGUREINTERFACE_H
