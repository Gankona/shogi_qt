#ifndef BATTLE_H
#define BATTLE_H

#include <Interface/Battle/informlabel.h>
#include <Interface/Battle/timerlabel.h>
#include <Interface/seguwidget.h>

#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtCore/QSettings>
#include <QtWidgets/QPushButton>

class Battle : public QObject{
    Q_OBJECT
public:
    Battle(/*QStringList &translate, */SeguWidget *menuWidget);
    InformLabel *informLabel;   //виджет на котором размещаются дополнительные возможности
    SeguWidget *mainWidget; //ссылка на главный обьект приложения
    TimerLabel *timerLabelPlayer,   //метка таймера игрока
                *timerLabelEnemy;   //противника

    QLabel *mainBattleLabel,    //виджет размещения доски с рукавами
            *board[9][9];   //масив 9х9 на которых распологаются фигуры
    QPushButton *switcherInformButton;  //кнопка откр\закр допольнительнной вкладки
    QSettings qsettings;    //файл настроек

    bool isFullScreen;  //будет ли игра на весь экран
    int razmX,  //размер игры вместе с допольнительной вкладкой информации, х-координаты
        razmY;  //у-координаты

    void readSettings();    //метод чтения настроек
    void createFullScreenInterface();   //создание полноэкранной версии игры
    void createWindowInterface();   //создание оконной версии игры

public slots:
    void slotClickOnBoard(QPoint pe);   //слот реакции на нажатие кнопки, возвращает координаты
    void slotSwitcherInformLabel(); //слот откр\закр допольнительной информации

signals:
    void signalClickOnBoard(int i, int j);  //сигнал возвращающий кнопки по которой попали
};

#endif // BATTLE_H
