#ifndef SETTINGS_H
#define SETTINGS_H

#include <QtCore/QFile>
#include <QtCore/QObject>
#include <QtCore/QSettings>

#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>

class Settings : public QObject{
    Q_OBJECT
public:
    Settings(QStringList &translate, QWidget *menuLabel);
    ~Settings();    //дискриптор

//Главные параметры
    QLabel  *mainSettingsLabel, //главная метка настроек накладывается на главный обьект
            *nameOfWindow;  //метка названия окна
    QPushButton *backButton;    //кнопка возвращения в меню
    QSettings qsettings;    //настройки

    int razmX, razmY;   //размер окна

    void createMainWindow();    //метод создания главного окна настроек
    void createBackButton();    //метод создания кнопки возвращения в меню
    void readSettings();    //метод чтения настроек с регистра

//Звуковые настройки
    QLabel  *soundName,
            *soundValue;
    QSlider *sliderSound;

    void soundSettings();   //метод обработки звуковых настроек

//Интерфейс смены языка
    QLabel  *languageName;
    QList<QPushButton *> buttonList;    //список доступных языков
    QStringList translatePack,  //языковой пакет выбранного языка
                languageChoose; //текущий язык

    void languageSettings();    //файл переопределения языка и замены интерфейса

//Меню настроек разрешения
    bool isFullScreen;  //
    int gameScreenX,    //размер игры
        gameScreenY;    //

    QLabel  *screenMenuSizeName,
            *screenGameSizeName,
            *screenMenuValue,   //отображение размеров меню
            *screenGameValue;   //и размеров игры
    QSlider *sliderMenuSize,    //ползунок изменения размеров меню
            *sliderGameSize;    //и игры
    QPushButton *fullScreenSwitchButton;    //кнопка переключатель полноэкранного/оконного режима

    void screenSizeSettings();  //метод обработки размеров игры и меню и их сохранение

public slots:
    void slotChangeLanguage();  //слот смены языка
    void slotChangeScreenMenuSize();    //слот смены размеров меню
    void slotChangeScreenGameSize();    //слот смены размеров игры
    void slotFullScreenSwitcher();  //слот переключателя полноэкранного режима
    void slotSaveSettings();    //слот сохранения настроек

signals:
    void signalBackAndSave();   //вызывает возвращения назад в меню и сохранение настроек
};

#endif // SETTINGS_H
