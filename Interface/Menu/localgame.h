#ifndef LOCALGAME_H
#define LOCALGAME_H

#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QLineEdit>

#include <Control/battlectrl.h>
#include <LocalGameServer/server.h>

class LocalGame : public QObject{
    Q_OBJECT
public:
    LocalGame(QStringList &translate, SeguWidget *menuLabel);
    ~LocalGame();   //деструктор

    int razmX,  //размер окна
        razmY;

    BattleCtrl *battleGame; //обьект игры
    Server *iAmServer;  //сервер

    QWidget *askNameServer; //окно ввода названия сервера при запуске сервера
    QLineEdit *nameofServer;    //поле воода названия сервера
    QHostAddress myAddresIP;    //IP адрес устройства
    QLabel  *infoAboutGames,    //метка выбора противника
            *localMenuLabel,    //собственно это окно
            *nameOfMenu;    //метка названия меню
    QList<QHostAddress> adressList; //список всех активных хостов в сети
    QList<QPushButton *> buttonList;    //список кнопок под эти адреса
    QPushButton *backToMenuButton,  //кнопка возвращения в меню
                *connectButton, //кнопка соединения с данным игроком
                *refreshSpisokPlayerButton; //кнопка обновления информации про доступных серверов
    QSettings qsettings;    //файлы настроек
    QString baseIP, //3/4 IP игрока
            firstButtonName;    //названия кнопки создания сервера
    QStringList translatePack;  //языковой пакет
    QTcpSocket *ss[255];    //сокеты которые "щупаеют" есть ли там сервер
    QTcpSocket *toSenderSocket;
    SeguWidget *mainMenuWidget; //ссылка на главное окно

    void createInterface(); //сощлаем интерфейс
    void findMyIP();    //определяем мой ip
    void readSettings();    //считывание настроек

public slots:
    void clickOnPlayerIP(); //описание реакции при нажатии на имя сервера
    void createServer();    //создание сервера
    void outputInfoAboutConnect();  //вывод информации про доступные соединения
    void refreshInfoAboutPlayers(); //обновление информации о соединениях
    void slotRegisteredNewConection();  //вызывается если находим доступное соединение
    void postFromServer();  //принимаем информацию от сервера
};

#endif // LOCALGAME_H
