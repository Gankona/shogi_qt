#include "mainmenu.h"

#include <QtCore/QDebug>
#include <QtCore/QFile>

MainMenu::MainMenu() : qsettings("fpro","Segu"){
    this->move(100,50);
    shadowLabel = new QLabel(this);

    //созлаем самые важные метки
    fakeLabel = new QLabel;
    mainLabel = new QLabel(shadowLabel);
    mainLabel->setText("v. 0.3.2  ");
    mainLabel->setAlignment(Qt::AlignRight|Qt::AlignTop);
    localGame = nullptr;
    settings = nullptr;

    //вызываем все методы для настройки для изменения настроек
    readSettings();
    createPunktMenu();
    readTranslateFile(nameOfLang);
    changeSizes();
}

//меняем размеры всех компонентов если меняется настройки или при инициализации
void MainMenu::changeSizes(){
    this->setFixedSize(razmX, razmY);

    menuLayout->setSpacing(razmX/20);
    menuLayout->setMargin(razmX/20);

    //установка градиента
    shadowLabel->setFixedSize(razmX, razmY);
    shadowLabel->setStyleSheet("background: qlineargradient( x1:0 y1:0.32, x2:0 y2:1, stop:0 white, stop:1 black); color: black");

    mainLabel->setFixedSize(this->width(),this->height());
    mainLabel->move(0,0);

    fakeLabel->setMaximumHeight(razmX*0.4);
    fakeLabel->setPixmap(QPixmap(":/picture/main.png"));
    fakeLabel->setScaledContents(true);

    for(int i = 0; i < 5; i++)
        pointMenu[i]->setMinimumHeight(razmX/9);
}

//генерация пунктов меню
void MainMenu::createPunktMenu(){
    fakeLabel = new QLabel;

    menuLayout = new QVBoxLayout;
    menuLayout->addWidget(fakeLabel);

    for (int i = 0; i<5; i++){
        pointMenu[i] = new QPushButton;
        menuLayout->addWidget(pointMenu[i]);
        pointMenu[i]->setFocusPolicy(Qt::NoFocus);
        pointMenu[i]->setStyleSheet("background: white; color: black");
        QObject::connect(pointMenu[i], SIGNAL(clicked()), SLOT(clickOnSomeButton()));
    }
    QObject::connect(pointMenu[4], SIGNAL(clicked()), this, SLOT(close()));

    mainLabel->setLayout(menuLayout);
}

//вызывается если другие окна что открывались поверх этой закрылись
void MainMenu::returnToMenu(){
    mainLabel->setVisible(true);
    readSettings();

    if (localGame != nullptr){
        delete localGame;
        localGame = nullptr;//обнулять всегда нудно ссылки чтоосвободил!
    }
    if (settings != 0){
        delete settings;
        settings = nullptr;
        changeSizes();
    }
}

//чтение нового языка что изменилась во время настроек
void MainMenu::readTranslateFile(QString typeLang){
    QFile file(":/Translate/"+typeLang+".txt");
    file.open(QIODevice::ReadOnly);
    QString str = "";
    translate.clear();
    for (int i = 0; i < 500; i++){
        str = file.readLine();
        translate += str.split('\n');
        translate[i] = translate[i*2];//при создании отсекаем перенос и приходится укорачивать в 2 раза
        if ((i > 1)&&(i < 7))
            pointMenu[i-2]->setText(translate[i]);//переименовать первые 7 кнопок меню
    }
}

//описание реакции на нажатие любой с 5 кнопок
void MainMenu::clickOnSomeButton(){
    QPushButton *select = (QPushButton*)sender();//извлекаем ссылку на кнопку что вызвала метод
//если нажато на один игрок
    if (select == pointMenu[0])
        ;
//если нажато на два игрка
    else if (select == pointMenu[1]){
        battleCtrl = new BattleCtrl(translate, this, 2, false, nullptr);
        //QObject::connect(this, SIGNAL(widgetReturned(QPoint)), battleCtrl->interfaceBattle, SLOT(slotClickOnBoard(QPoint)));
    }
//если нажато на кнопку сети
    else if (select == pointMenu[2]){
        localGame = new LocalGame(translate, this);
        QObject::connect(localGame->backToMenuButton, SIGNAL(clicked()), SLOT(returnToMenu()));
    }
//если надато на настройки
    else if (select == pointMenu[3]){
        settings = new Settings(translate, this);
        QObject::connect(settings, SIGNAL(signalBackAndSave()), SLOT(returnToMenu()));
        for (int i = 0; i < settings->buttonList.length(); i++)
            QObject::connect(settings->buttonList.at(i), SIGNAL(clicked()), SLOT(changeGlobalLanguage()));
    }
    mainLabel->setVisible(false);//так как открыли новок окно старое делаем невидимым
}

//изменение языка
void MainMenu::changeGlobalLanguage(){
    QPushButton *select = (QPushButton*)sender();
    for (int  i = 0; i < settings->buttonList.length(); i++)
        if (settings->buttonList.at(i) == select){
            QFile file(":/Translate/"+settings->languageChoose.at(i)+".txt");
            file.open(QIODevice::ReadOnly);
            QString str = "";
            translate.clear();
            for (int i = 0; i < 500; i++){
                str = file.readLine();
                translate += str.split('\n');
                translate[i] = translate[i*2];
                if ((i > 1)&&(i < 7))
                    pointMenu[i-2]->setText(translate[i]);
            }
        }
}

//считываем настройки игры с регистра
void MainMenu::readSettings(){
    razmX = qsettings.value("/Settings/menuInterface/razmerMenuX", "245").toInt();
    razmY = qsettings.value("/Settings/menuInterface/razmerMenuY", "405").toInt();
    nameOfLang = qsettings.value("/Settings/language", "EN").toString();
}
