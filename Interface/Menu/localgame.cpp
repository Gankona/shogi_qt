#include "localgame.h"
#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QTime>

LocalGame::LocalGame(QStringList &translate, SeguWidget *menuLabel): qsettings("fpro","Segu"){
    mainMenuWidget = menuLabel;
    translatePack = translate;
    iAmServer = nullptr;

    //создаем 255 сокетов которые парсят возможные соединения
    localMenuLabel = new QLabel(mainMenuWidget);
    for (int i = 0; i<255; i++)
        ss[i] = new QTcpSocket(this);

    readSettings();
    createInterface();
    findMyIP();
    refreshInfoAboutPlayers();

    QObject::connect(refreshSpisokPlayerButton, SIGNAL(clicked()), SLOT(refreshInfoAboutPlayers()));
}

//деструктор
LocalGame::~LocalGame(){
    delete backToMenuButton;
    delete refreshSpisokPlayerButton;
    delete connectButton;
    delete infoAboutGames;
    delete nameOfMenu;
    delete localMenuLabel;
    qDebug() << "delete local";
}

//чтение настроек
void LocalGame::readSettings(){
    qsettings.beginGroup("/Settings/menuInterface");
    razmX = qsettings.value("/razmerMenuX", "255").toInt();
    razmY = qsettings.value("/razmerMenuY", "405").toInt();
    qsettings.endGroup();
}

//создание интерфейса окна сетевой игры
void LocalGame::createInterface(){
    localMenuLabel->setFixedSize(razmX,razmY);
    localMenuLabel->move(0,0);
    
    //верхняя подпись "что за окно"
    nameOfMenu = new QLabel(translatePack.at(4), localMenuLabel);
    nameOfMenu->setFixedSize(razmX,25);
    nameOfMenu->setAlignment(Qt::AlignCenter);

    //тут отображается список всех активных соединений
    infoAboutGames = new QLabel(localMenuLabel);
    infoAboutGames->setFixedSize(razmX*14/15, razmY-75);
    infoAboutGames->move(razmX/30, 25);
    
    //кнопка соединения с выбранным сервером
    connectButton = new QPushButton(translatePack.at(7), localMenuLabel);
    connectButton->setFixedSize(razmX*9/31, 26);
    connectButton->move(razmX/31, razmY-45);
    connectButton->setStyleSheet("background: white; color: black");
    connectButton->setFocusPolicy(Qt::NoFocus);

    //обновляет список доступных соединений
    refreshSpisokPlayerButton = new QPushButton(translatePack.at(8), localMenuLabel);
    refreshSpisokPlayerButton->setFixedSize(razmX*9/31, 26);
    refreshSpisokPlayerButton->move(razmX*11/31, razmY-45);
    refreshSpisokPlayerButton->setStyleSheet("background: white; color: black");
    refreshSpisokPlayerButton->setFocusPolicy(Qt::NoFocus);

    //кнопка возврата в предыдущее меню
    backToMenuButton = new QPushButton(translatePack.at(9), localMenuLabel);
    backToMenuButton->setFixedSize(razmX*9/31, 26);
    backToMenuButton->move(razmX*21/31, razmY-45);
    backToMenuButton->setStyleSheet("background: white; color: black");
    backToMenuButton->setFocusPolicy(Qt::NoFocus);

    //перерисовка всего этого окна
    localMenuLabel->show();
}

//метод поиска своего IP и создания базиса с него путем отрезание последнего значения
void LocalGame::findMyIP(){
    bool isFind = false;
    QList<QHostAddress> list = QNetworkInterface::allAddresses();
    for (int i = 0; (i < list.length())&&(!isFind); i++){
        int kolPoint = 0;
        for (int j = 0; j < list.at(i).toString().length(); j++){
            if (list.at(i).toString().at(j) == '.')
                kolPoint++;
        }
        if (kolPoint == 3){
            if (list.at(i).toString() == "127.0.0.1")
                myAddresIP = "127.0.0.1";
            else{
                myAddresIP = list.at(i);
                isFind = true;
            }
        }
    }
    baseIP = "";
    QString str = myAddresIP.toString();
    for (int i = 0, j = 0; i < 3; j++){
        if (str[j] == '.')
            i++;
        baseIP += str[j];
    }
    qDebug() << baseIP;
}

//обновление информации про соединения
void LocalGame::refreshInfoAboutPlayers(){
    qDebug() << "start at " << QTime::currentTime();
    for (int i = 0; i<255; i++){//очистка памяти что бы не захламлялось
        delete ss[i];
        ss[i] = nullptr;//обязательно обнулить ссылку
        ss[i] = new QTcpSocket(this);
        //если новое соединение тогда вызываем первый слот, если клиент что то присылает то второй
        QObject::connect(ss[i], SIGNAL(connected()) , SLOT(slotRegisteredNewConection()));
        QObject::connect(ss[i], SIGNAL(readyRead()), SLOT(postFromServer()));
    }
    //пробуем подключится на все 255 хостов сразу
    for (int i = 0; i<255; i++)
        ss[i]->connectToHost(baseIP+QString::number(i), 42015);

    //вывод информации о текущем соединении(выведет только сам себя тут)
    outputInfoAboutConnect();//));
}

//вывод информации про соединения
void LocalGame::outputInfoAboutConnect(){
    if (!buttonList.isEmpty()){//проверка на первый запуск, если не первый тогда удаляем кнопки
        //что бы потом заного их создать и там прописать все возмодные соединения
        firstButtonName = buttonList.at(0)->text();
        for (int i = 0; i < buttonList.length(); i++)
            delete buttonList.at(i);
    }
    else firstButtonName = translatePack.at(10);

    //очистка стека кнопок и адресов и размещение кнопки создания сервера
    buttonList.clear();
    adressList.clear();
    buttonList.push_back(new QPushButton(firstButtonName, infoAboutGames));

    //смотрим если на данном сокете говорится что он соединился и слушает порт 42015 то отображаем
    for (int i = 0; i<255; i++)
        if (ss[i] != nullptr){
            if (ss[i]->peerPort() == 42015){
                buttonList.push_back(new QPushButton(ss[i]->peerAddress().toString(), infoAboutGames));
                adressList.push_back(ss[i]->peerAddress());
            }
        }

    //настройка кнопок с названием возможных соединений
    for (int i = 0; i < buttonList.length(); i++){
        buttonList.at(i)->move(0, i*30);
        buttonList.at(i)->setFixedSize(razmX*14/15, 30);
        buttonList.at(i)->setFlat(true);
        buttonList.at(i)->setFocusPolicy(Qt::NoFocus);
        buttonList.at(i)->show();
    }

    //если нажмем на любую кнопку в списке подключений вызвется метод clickOnPlayerIP()
    for (int i = 0; i < buttonList.length(); i++){
        QObject::connect(buttonList.at(i), SIGNAL(clicked()), SLOT(clickOnPlayerIP()));
    }
}

//ожидает информацию что пришлется с сервера, в нашем случае ждет пока пришлется имя сервера
void LocalGame::postFromServer(){
    QTextStream in((QTcpSocket*)sender());
    QString serverName = in.readAll();
    qDebug() << "Имя сервера: " << serverName;
    for (int i = 0; i < 255; i++)
        if ((QTcpSocket*)sender() == ss[i])
            for (int j = 0; j < adressList.length(); j++)
                if (adressList.at(j) == ss[i]->peerAddress())
                    buttonList.at(j+1)->setText(serverName);//подписуем в списке подключений
}

//если обнаружен новый сервер тогда вызываем обновление информации про соединения
void LocalGame::slotRegisteredNewConection(){
    for (int i = 0; i < 255; i++)
        if ((QTcpSocket*)sender() == ss[i])
            qDebug() << i << " found at " << QTime::currentTime();
    outputInfoAboutConnect();
}

//реакция на нажатие одной с кнопок соединений
void LocalGame::clickOnPlayerIP(){
    QPushButton *select = (QPushButton*)sender();//запоминаем кто вызвал этот метод
    if (select == buttonList.at(0)){//нажимаем на кнопку создания/удаления сервера
        if (iAmServer == nullptr){
            //создаем окно которое просит ввести имя сервера
            askNameServer = new QWidget;
            askNameServer->setWindowFlags(Qt::FramelessWindowHint);
            askNameServer->setWindowModality(Qt::WindowModal);
            askNameServer->setFixedSize(210,55);
            askNameServer->move(mainMenuWidget->x()+0.1*mainMenuWidget->width(),
                                mainMenuWidget->y()+0.4*mainMenuWidget->height());
            QLabel *string = new QLabel(translatePack.at(18), askNameServer);
            string->setFixedSize(210,20);
            string->move(10,0);
            nameofServer = new QLineEdit(askNameServer);
            nameofServer->setFixedSize(190, 28);
            nameofServer->move(10, 21);
            askNameServer->show();

            //при вводе имени и нажатии Enter имя сохраняется
            QObject::connect(nameofServer, SIGNAL(editingFinished()), SLOT(createServer()));
        }
        else {//останавливаем сервер
            delete iAmServer;   //удаляем обьект сервера
            iAmServer = nullptr;    //обнуляем указатель  на сервер
            buttonList.at(0)->setText(translatePack.at(10));
            while (buttonList.first() != buttonList.last()){
                delete buttonList.last();
                buttonList.removeLast();
            }
        }
    }
    else    //если нажали на соединение
        if (iAmServer == nullptr)   //проверка что бы сервер и клиент были не одно лицо
            for (int i = 1; i < buttonList.length(); i++)
                if (buttonList.at(i) == select){//поиск адреса на который нажали
                    for (int j = 0; j < 255; j++)
                        if (ss[j]->peerAddress() == adressList.at(i-1))
                            toSenderSocket = ss[j];
                    QTextStream start(toSenderSocket);  //создаем текстовый поток для отправки на сокет
                    start << "start";   //отправляем текст для начала игры на сервер, что бы он тоже стартанул
                    qDebug() << "отправлена информация что бы начать игру";
                    //создание окна игры
                    battleGame = new BattleCtrl(translatePack, mainMenuWidget, 0, false, toSenderSocket);
                    //отсоединение ожидания информации от сервера, что бы во время игры не перехватывало передачу
                    QObject::disconnect(toSenderSocket, SIGNAL(readyRead()), this, SLOT(postFromServer()));
                }
}

//создаем сервер
void LocalGame::createServer(){
    qDebug() << "начинаем создавать сервер";
    iAmServer = new Server(this, nameofServer->text(), translatePack, mainMenuWidget);
    buttonList.at(0)->setText(translatePack.at(11)); //пишем вместо создать, остановить сервер
    qDebug() << "создали сервер";
    refreshInfoAboutPlayers();//обновление информации про соединения
    delete nameofServer;    //удаляем имя сервера
    delete askNameServer;   //удаляем окно вопроса про имя сервера
}
