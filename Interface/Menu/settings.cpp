#include "settings.h"
#include <QDebug>
#define KOL 11

Settings::Settings(QStringList &translate, QWidget *menuLabel): qsettings("fpro","Segu"){
    readSettings();
    translatePack = translate;
    mainSettingsLabel = new QLabel(menuLabel);

    //вызов методов настройки меню
    createMainWindow();
    soundSettings();
    languageSettings();
    screenSizeSettings();
    createBackButton();

    //необходимо для перерисовки окна настроек
    mainSettingsLabel->show();
}

//дескриптор, он вроде бы как не нужен, но чего то без него не работает
Settings::~Settings(){
    delete nameOfWindow;
    delete backButton;
    delete sliderSound;
    delete soundName;
    delete soundValue;
    delete languageName;
    for (int i = 0; i < languageChoose.length(); i++)
        delete buttonList.at(i);
    delete mainSettingsLabel;
    qDebug() << "delete settings";
}

//считываение сохраненных настроек с регистра
void Settings::readSettings(){
    qsettings.beginGroup("/Settings/menuInterface");
    razmX = qsettings.value("/razmerMenuX", "255").toInt();
    razmY = qsettings.value("/razmerMenuY", "405").toInt();
    qsettings.endGroup();
}

//метод создания главного окна настроек
void Settings::createMainWindow(){
    mainSettingsLabel->setFixedSize(razmX, razmY);
    mainSettingsLabel->setStyleSheet("color: black");

    nameOfWindow = new QLabel(translatePack.at(5), mainSettingsLabel);
    nameOfWindow->setAlignment(Qt::AlignCenter);
    nameOfWindow->setFixedSize(razmX-20, (razmY-20-(5*KOL))/KOL);
    nameOfWindow->move(10,10);
}

//создание и настройка кнопки возврата в меню
void Settings::createBackButton(){
    backButton = new QPushButton(translatePack.at(9), mainSettingsLabel);
    backButton->setFocusPolicy(Qt::NoFocus);
    backButton->setFixedSize(razmX-20, (razmY-20-(5*KOL))/KOL);
    backButton->move(10, razmY-10-backButton->height());\
    backButton->setStyleSheet("background: white; color: black");
    QObject::connect(backButton, SIGNAL(clicked()), SLOT(slotSaveSettings()));
}

//настройки звука
void Settings::soundSettings(){
    soundName = new QLabel(translatePack.at(12), mainSettingsLabel);
    soundName->setAlignment(Qt::AlignVCenter);
    soundName->setFixedSize(razmX-20, (razmY-20-(5*KOL))/KOL);
    soundName->move(10, nameOfWindow->y()+nameOfWindow->height()+5);
    sliderSound = new QSlider(Qt::Horizontal, mainSettingsLabel);
    sliderSound->setFocusPolicy(Qt::NoFocus);
    sliderSound->setTickInterval(5);
    sliderSound->setTickPosition(QSlider::TicksBelow);
    sliderSound->setRange(0, 100);
    sliderSound->setFixedSize((razmX-10)*0.8, (razmY-20-(5*KOL))/KOL);
    sliderSound->move(10, soundName->y()+soundName->height()+5);
    if (qsettings.value("/Settings/volume").toString().length() == 0)
        sliderSound->setValue(100);
    else sliderSound->setValue(qsettings.value("/Settings/volume").toInt());
    soundValue = new QLabel(QString::number(sliderSound->value()), mainSettingsLabel);
    soundValue->setAlignment(Qt::AlignCenter);
    soundValue->setFixedSize((razmX-10)*0.15, (razmY-20-(5*KOL))/KOL);
    soundValue->move(razmX-10-soundValue->width(), sliderSound->y());
    QObject::connect(sliderSound, SIGNAL(valueChanged(int)), soundValue, SLOT(setNum(int)));
}

//настройка языка
void Settings::languageSettings(){
    languageName = new QLabel(translatePack.at(13), mainSettingsLabel);
    languageName->setAlignment(Qt::AlignBottom);
    languageName->setFixedSize(razmX-20, (razmY-20-(5*KOL))/KOL);
    languageName->move(10, sliderSound->y()+sliderSound->height()+5);
    languageChoose.clear();
    languageChoose << "EN" << "RU" << "UA"; //можно добавить новые языки тут(не забыть про текстовый файл)
    for (int i = 0; i < languageChoose.length(); i++){
        buttonList.push_back(new QPushButton(languageChoose.at(i), mainSettingsLabel));
        buttonList.at(i)->setFocusPolicy(Qt::NoFocus);
        buttonList.at(i)->setFixedSize((razmY-20-(5*KOL))/KOL, (razmY-20-(5*KOL))/KOL);
        buttonList.at(i)->move(20+(10+buttonList.at(i)->height())*i, languageName->y()+languageName->height()+5);
        QObject::connect(buttonList.at(i), SIGNAL(clicked()), SLOT(slotChangeLanguage()));
    }
}

//при выборе нового языка считывания содержимого по данному языку
void Settings::slotChangeLanguage(){
    QPushButton *select = (QPushButton*)sender();
    for (int  i = 0; i < buttonList.length(); i++)
        if (buttonList.at(i) == select){
            qsettings.setValue("/Settings/language", languageChoose.at(i));
            QFile file(":/Translate/"+languageChoose.at(i)+".txt");
            file.open(QIODevice::ReadOnly);
            QString str = "";
            translatePack.clear();
            for (int i = 0; i < 500; i++){
                str = file.readLine();
                translatePack += str.split('\n');
                translatePack[i] = translatePack[i*2];
            }
        }
    //переименование названий в этом окне по новому выбору языка
    nameOfWindow->setText(translatePack.at(4));
    backButton->setText(translatePack.at(9));
    soundName->setText(translatePack.at(12));
    languageName->setText(translatePack.at(13));
    screenGameSizeName->setText(translatePack.at(15));
    screenMenuSizeName->setText(translatePack.at(14));
    if (isFullScreen)
        fullScreenSwitchButton->setText(translatePack.at(17));
    else
        fullScreenSwitchButton->setText(translatePack.at(16));
}

//настройка размеров игры и меню
void Settings::screenSizeSettings(){
//настройка размеров меню
    screenMenuSizeName = new QLabel(translatePack.at(14), mainSettingsLabel);
    screenMenuSizeName->setAlignment(Qt::AlignBottom);
    screenMenuSizeName->setFixedSize(razmX-20, (razmY-20-(5*KOL))/KOL);
    screenMenuSizeName->move(10, buttonList.at(0)->y()+buttonList.at(0)->height()+5);
    sliderMenuSize = new QSlider(Qt::Horizontal, mainSettingsLabel);
    sliderMenuSize->setFocusPolicy(Qt::NoFocus);
    sliderMenuSize->setTickInterval(50);
    sliderMenuSize->setTickPosition(QSlider::TicksBelow);
    sliderMenuSize->setRange(250,400);
    sliderMenuSize->setValue(razmX);
    sliderMenuSize->setFixedSize((razmX-10)*0.8, (razmY-20-(5*KOL))/KOL);
    sliderMenuSize->move(10, screenMenuSizeName->y()+screenMenuSizeName->height()+5);
    screenMenuValue = new QLabel(QString::number(razmX)+"\n"+QString::number(razmY), mainSettingsLabel);
    screenMenuValue->setAlignment(Qt::AlignCenter);
    screenMenuValue->setFixedSize((razmX-10)*0.15, (razmY-20-(5*KOL))/KOL);
    screenMenuValue->move(razmX-10-screenMenuValue->width(), sliderMenuSize->y());
    QObject::connect(sliderMenuSize, SIGNAL(valueChanged(int)), SLOT(slotChangeScreenMenuSize()));

//настройка размеров игры
    screenGameSizeName = new QLabel(translatePack.at(15), mainSettingsLabel);
    screenGameSizeName->setAlignment(Qt::AlignBottom);
    screenGameSizeName->setFixedSize(razmX-20, (razmY-20-(5*KOL))/KOL);
    screenGameSizeName->move(10, sliderMenuSize->y()+sliderMenuSize->height()+5);
    sliderGameSize = new QSlider(Qt::Horizontal, mainSettingsLabel);
    sliderGameSize->setFocusPolicy(Qt::NoFocus);
    sliderGameSize->setTickInterval(50);
    sliderGameSize->setTickPosition(QSlider::TicksBelow);
    sliderGameSize->setRange(400,1100);
    if (qsettings.value("/Settings/gameInterface/razmerGameX").toString().length() == 0)
        gameScreenX = 300;
    else
        gameScreenX = qsettings.value("/Settings/gameInterface/razmerGameX").toInt();
    gameScreenY = gameScreenX*0.63;
    sliderGameSize->setValue(gameScreenX);
    sliderGameSize->setFixedSize((razmX-10)*0.8, (razmY-20-(5*KOL))/KOL);
    sliderGameSize->move(10, screenGameSizeName->y()+screenGameSizeName->height()+5);
    screenGameValue = new QLabel(mainSettingsLabel);
    screenGameValue->setText(QString::number(gameScreenX)+'\n'+QString::number(gameScreenY));
    screenGameValue->setAlignment(Qt::AlignCenter);
    screenGameValue->setFixedSize((razmX-10)*0.15, (razmY-20-(5*KOL))/KOL);
    screenGameValue->move(razmX-10-screenGameValue->width(), sliderGameSize->y());
    QObject::connect(sliderGameSize, SIGNAL(valueChanged(int)), SLOT(slotChangeScreenGameSize()));

//настройка возможности полноэкранной игры
    fullScreenSwitchButton = new QPushButton(mainSettingsLabel);
    fullScreenSwitchButton->setFocusPolicy(Qt::NoFocus);
    fullScreenSwitchButton->setFixedSize(razmX-20, (razmY-20-(5*KOL))/KOL);
    fullScreenSwitchButton->move(10, sliderGameSize->y()+sliderGameSize->height()+5);
    fullScreenSwitchButton->setStyleSheet("background: white; color: black");
    if (qsettings.value("/Settings/gameInterface/isFullScreen").toString().length() == 0)
        isFullScreen = false;
    else
        isFullScreen = qsettings.value("/Settings/gameInterface/isFullScreen").toBool();
    if (isFullScreen)
        fullScreenSwitchButton->setText(translatePack.at(17));
    else
        fullScreenSwitchButton->setText(translatePack.at(16));
    QObject::connect(fullScreenSwitchButton, SIGNAL(clicked()), SLOT(slotFullScreenSwitcher()));
}

//метод запоминания размеров игры
void Settings::slotChangeScreenGameSize(){
    gameScreenX = sliderGameSize->value();
    gameScreenY = gameScreenX*0.63;
    screenGameValue->setText(QString::number(gameScreenX)+'\n'+QString::number(gameScreenY));
}

//метод запоминания размеров меню
void Settings::slotChangeScreenMenuSize(){
    razmX = sliderMenuSize->value();
    razmY = razmX/0.63;
    screenMenuValue->setText(QString::number(razmX)+'\n'+QString::number(razmY));
}

//переключатель полноекранный/оконный режим
void Settings::slotFullScreenSwitcher(){
    if (isFullScreen){
        isFullScreen = false;
        fullScreenSwitchButton->setText(translatePack.at(16));
    }
    else {
        isFullScreen = true;
        fullScreenSwitchButton->setText(translatePack.at(17));
    }
}

//вызывается при нажатии возврата в меню при этом сохраняет настройки
void Settings::slotSaveSettings(){
    qsettings.beginGroup("/Settings");
    qsettings.setValue("/volume", sliderSound->value());
    qsettings.setValue("/menuInterface/razmerMenuX", razmX);
    qsettings.setValue("/menuInterface/razmerMenuY", razmY);
    qsettings.setValue("/gameInterface/razmerGameX", gameScreenX);
    qsettings.setValue("/gameInterface/razmerGameY", gameScreenY);
    qsettings.setValue("/gameInterface/isFullScreen", isFullScreen);
    qsettings.endGroup();
    emit signalBackAndSave();
}
