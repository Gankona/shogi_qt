#ifndef MAINMENU_H
#define MAINMENU_H

#include <Interface/seguwidget.h>
#include <Interface/Menu/localgame.h>
#include <Interface/Menu/settings.h>

#include <QtCore/QObject>
#include <QtWidgets/QLabel>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

class MainMenu : public SeguWidget{
    Q_OBJECT
public:
    MainMenu();
    BattleCtrl *battleCtrl; //обьект для управления игрой
    LocalGame *localGame;   //окно локальной сети
    Settings *settings; //окно настроек

    QLabel *mainLabel,  //окно меню
            *fakeLabel, //надпись названия игры
            *shadowLabel;   //лейбл для фона меню
    QPushButton *pointMenu[5];  //5 кнопок в меню
    QSettings qsettings;
    QString nameOfLang; //текущий язык
    QStringList translate;  //языковой пакет
    QVBoxLayout *menuLayout;    //компоновщик в главном меню

    int razmX,  //размер окна
        razmY;

    void changeSizes(); //метод автоматического изменения размеров при изменении настроек
    void createPunktMenu(); //метод создания кнопок в меню
    void readSettings();    //чтение настроек игры и параметров меню
    void readTranslateFile(QString typeLang);   //заполняем языковой пакет выбраным языком

public slots:
    void changeGlobalLanguage();    //изменение языка интерфейса
    void clickOnSomeButton();   //описания реакции на нажатие кнопок в меню
    void returnToMenu();    //функция закрытия открытых окон и возвращения в меню
};

#endif // MAINMENU_H
