#ifndef SEGUWIDGET_H
#define SEGUWIDGET_H

#include <QtGui/QMouseEvent>
#include <QtWidgets/QWidget>

class SeguWidget : public QWidget{
    Q_OBJECT
public:
    explicit SeguWidget(QWidget *parent = 0);
    virtual void mouseReleaseEvent(QMouseEvent *pe);    //переопределенный метод реакции на нажатие

signals:
    int widgetReturned(QPoint x);   //возвращает координаты нажатия
};

#endif // SEGUWIDGET_H
